<?php

class Insert_model extends CI_Model
{
    // USER
    // --------------------------------------------------------------------------------------------------
    public function addUser($data)
    {
        $this->db->insert('user', $data);
        return $this->db->affected_rows();
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOKS
    // --------------------------------------------------------------------------------------------------
    public function addBook($data)
    {
        $this->db->insert('books', $data);
        return $this->db->affected_rows();
    }
    // END BOOKS
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function addCategory($data)
    {
        $this->db->insert('category', $data);
        return $this->db->affected_rows();
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function addIndustry($data)
    {
        $this->db->insert('industry', $data);
        return $this->db->affected_rows();
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function addRole($data)
    {
        $this->db->insert('role', $data);
        return $this->db->affected_rows();
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // MESSAGE
    // --------------------------------------------------------------------------------------------------
    public function addMessage($data)
    {
        $this->db->insert('messages', $data);
        return $this->db->affected_rows();
    }
    // END MESSAGE
    // --------------------------------------------------------------------------------------------------

    // like N dislike
    // --------------------------------------------------------------------------------------------------
    public function likeNDislike($data)
    {
        $this->db->insert('likes', $data);
        return $this->db->affected_rows();
    }

    public function bookmark($data)
    {
        $this->db->insert('bookmark', $data);
        return $this->db->affected_rows();
    }
    // END like N dislike
    // --------------------------------------------------------------------------------------------------
}
