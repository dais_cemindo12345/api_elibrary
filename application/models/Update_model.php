<?php

class Update_model extends CI_Model
{
    // USER
    // --------------------------------------------------------------------------------------------------
    public function updateUser($data)
    {
        $this->db->where('user_id', $data['user_id']);
        $this->db->update('user', $data);
        return $this->db->affected_rows();
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOK
    // --------------------------------------------------------------------------------------------------
    public function updateBook($data)
    {
        $this->db->where('book_id', $data['book_id']);
        $this->db->update('books', $data);
        return $this->db->affected_rows();
    }
    // END BOOK
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function updateCategory($data)
    {
        $this->db->where('category_id', $data['category_id']);
        $this->db->update('category', $data);
        return $this->db->affected_rows();
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function updateIndustry($data)
    {
        $this->db->where('industry_id', $data['industry_id']);
        $this->db->update('industry', $data);
        return $this->db->affected_rows();
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function updateRole($data)
    {
        $this->db->where('role_id', $data['role_id']);
        $this->db->update('role', $data);
        return $this->db->affected_rows();
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------

    // MESSAGE
    // --------------------------------------------------------------------------------------------------
    public function updateMessage($data)
    {
        $this->db->where('message_id', $data['message_id']);
        $this->db->update('messages', $data);
        return $this->db->affected_rows();
    }
    // END MESSAGE
    // --------------------------------------------------------------------------------------------------
}
