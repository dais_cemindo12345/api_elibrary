<?php

class Delete_model extends CI_Model
{

    // USER
    // --------------------------------------------------------------------------------------------------
    public function deleteUser($id)
    {
        $this->db->delete('user', ['user_id' => $id]);
        return $this->db->affected_rows();
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOK
    // --------------------------------------------------------------------------------------------------
    public function deleteBook($id)
    {
        $this->db->delete('books', ['book_id' => $id]);
        return $this->db->affected_rows();
    }
    // END BOOK
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function deleteCategory($id)
    {
        $this->db->delete('category', ['category_id' => $id]);
        return $this->db->affected_rows();
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function deleteIndustry($id)
    {
        $this->db->delete('industry', ['industry_id' => $id]);
        return $this->db->affected_rows();
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function deleteRole($id)
    {
        $this->db->delete('role', ['role_id' => $id]);
        return $this->db->affected_rows();
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------

    // MESSAGE
    // --------------------------------------------------------------------------------------------------
    public function deleteMessage($id)
    {
        $this->db->delete('messages', ['message_id' => $id]);
        return $this->db->affected_rows();
    }
    // END MESSAGE
    // --------------------------------------------------------------------------------------------------

    // like N dislike
    // --------------------------------------------------------------------------------------------------
    public function likeNDislike($data)
    {
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('book_id', $data['book_id']);
        $this->db->delete('likes');
        return $this->db->affected_rows();
    }

    public function bookmark($data)
    {
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('book_id', $data['book_id']);
        $this->db->delete('bookmark');
        return $this->db->affected_rows();
    }
    // END like N dislike
    // --------------------------------------------------------------------------------------------------
}
