<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Read extends RestController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    // USER
    // --------------------------------------------------------------------------------------------------
	public function cek_post()
    {
        echo "OK";
    }    

public function showUser_post()
    {
        $this->db->order_by('user_id', 'DESC');
        $data = $this->db->get('user')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showUserById_post()
    {
        $id = $this->post('user_id');
        $data = $this->db->get_where('user', ['user_id' => $id])->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function userLogin_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');

        $this->db->where('username', $username);
        $user = $this->db->get('user')->row_array();
        if ($user) {
            if (password_verify($password, $user['password'])) {
                $data = [
                    'user_id' => $user['user_id'],
                    'username' => $user['username'],
                    'role_id' => $user['role_id'],
                ];

                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'result' => $data
                ], 200);
            } else {
                $data = "Password salah";
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'result' => $data
                ], 404);
            }
        } else {
            $data = "Nama Pengguna Tidak Terdaftar";

            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => $data
            ], 404);
        }
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOK
    // --------------------------------------------------------------------------------------------------
    public function showBook_post()
    {
        $this->db->order_by('book_id', 'DESC');
        $data = $this->db->get('books')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showBookJoin_post()
    {
        $query = "SELECT B.*, COUNT(L.book_id) AS Counter, L.like_status FROM books B
        LEFT JOIN likes L ON B.book_id = L.book_id
        LEFT JOIN user U ON L.user_id = U.user_id
        GROUP BY B.book_id
        ORDER BY B.book_id DESC ";

        $data = $this->db->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showBookJoinFavorit_post()
    {
        $query = "SELECT B.*, COUNT(L.book_id) AS Counter FROM books B
        LEFT JOIN likes L ON B.book_id = L.book_id
        LEFT JOIN user U ON L.user_id = U.user_id
        GROUP BY B.book_id
        ORDER BY Counter DESC ";

        $data = $this->db->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showBookLikeByUserId_post()
    {
        $id = $this->post('user_id');

        $query = "SELECT B.*, COUNT(L.book_id) AS Counter FROM books B
        LEFT JOIN likes L ON B.book_id = L.book_id
        WHERE L.user_id = '{$id}'
        GROUP BY B.book_id
        ORDER BY B.book_id DESC ";

        $data = $this->db->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showBookCollectionByUserId_post()
    {
        $id = $this->post('user_id');

        $query = "SELECT B.*, COUNT(M.book_id) AS Counter FROM books B
        LEFT JOIN bookmark M ON B.book_id = M.book_id
        WHERE M.user_id = '{$id}'
        GROUP BY B.book_id
        ORDER BY B.book_id DESC ";

        $data = $this->db->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showBookById_post()
    {
        $id = $this->post('book_id');
        $data = $this->db->get_where('books', ['book_id' => $id])->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END BOOK
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function showCategory_post()
    {
        $data = $this->db->get('category')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showCategoryById_post()
    {
        $id = $this->post('category_id');
        $data = $this->db->get_where('category', ['category_id' => $id])->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function showIndustry_post()
    {
        $data = $this->db->get('industry')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showIndustryById_post()
    {
        $id = $this->post('industry_id');
        $data = $this->db->get_where('industry', ['industry_id' => $id])->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function showRole_post()
    {
        // $header = getallheaders();

        // if ($header["Authorization"] == "Bearer e783df8c-7b99-4591-bd04-7d04d0718b85") {
        // } else {
        //     // Set the response and exit
        //     $this->response([
        //         'status' => FALSE,
        //         'result' => 'Data tidak ditemukan'
        //     ], 404);
        // }

        $data = $this->db->get('role')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showRoleById_post()
    {
        $id = $this->post('role_id');
        $data = $this->db->get_where('role', ['role_id' => $id])->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------

    // result
    // --------------------------------------------------------------------------------------------------
    public function showresult_post()
    {
        $data = $this->db->get('results')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showresultById_post()
    {
        $id = $this->post('result_id');
        $data = $this->db->get_where('results', ['result_id' => $id])->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END result
    // --------------------------------------------------------------------------------------------------

    // LIKE
    // --------------------------------------------------------------------------------------------------
    public function showLike_post()
    {
        $data = $this->db->get('likes')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showLikeCount_post()
    {
        $data = $this->db->get('likes')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showLikeByUserId_post()
    {
        $id = $this->post('user_id');
        $data = $this->db->get_where('likes', ['user_id' => $id])->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END LIKE
    // --------------------------------------------------------------------------------------------------

    // BOOKMARK
    // --------------------------------------------------------------------------------------------------
    public function showBookmark_post()
    {
        $data = $this->db->get('bookmark')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function showBookmarkByUserId_post()
    {
        $id = $this->post('user_id');
        $data = $this->db->get_where('bookmark', ['user_id' => $id])->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    // END BOOKMARK
    // --------------------------------------------------------------------------------------------------
}
