<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Filter extends RestController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // $this->load->model('Filter_model', 'filter');
    }

    // BOOK
    // --------------------------------------------------------------------------------------------------
    public function filterBook_post()
    {
        $book_industry = $this->post('book_industry');

        // $this->db->order_by('book_id', 'DESC');
        // $data = $this->db->get_where('books', ['book_industry' => $book_industry])->result_array();

        $query = "SELECT B.*, COUNT(L.book_id) AS Counter FROM books B
        LEFT JOIN likes L ON B.book_id = L.book_id
        LEFT JOIN user U ON L.user_id = U.user_id
        WHERE B.book_industry = $book_industry
        GROUP BY B.book_id
        ORDER BY B.book_id DESC";

        $data = $this->db->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'User tidak ditemukan'
            ], 404);
        }
    }
    // END BOOK
    // --------------------------------------------------------------------------------------------------
}
