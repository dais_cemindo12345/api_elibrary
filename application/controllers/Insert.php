<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Insert extends RestController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Insert_model', 'I');
        $this->load->model('Delete_model', 'D');
    }

    // USER
    // --------------------------------------------------------------------------------------------------
    public function addUser_post()
    {
        // cek username
        $cekUser = $this->db->get_where('user', ['username' => $this->post('username')])->num_rows();
        $cekEmail = $this->db->get_where('user', ['email' => $this->post('email')])->num_rows();
        if ($cekUser) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'User sudah terdaftar'
            ], 404);
        } elseif ($cekEmail) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Email sudah terdaftar'
            ], 404);
        } else {
            // password hash
            $password = password_hash($this->post('password'), PASSWORD_DEFAULT);

            $data = [
                'username' => $this->post('username'),
                'password' => $password,
                'email' => $this->post('email'),
                'phone_number' => $this->post('phone_number'),
                'user_image' => $this->post('user_image'),
                'date_created' => $this->post('date_created'),
                'user_created' => $this->post('user_created'),
                'role_id' => $this->post('role_id'),
            ];


            if ($this->I->addUser($data) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User berhasil ditambahkan'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'User gagal ditambahkan'
                ], 404);
            }
        }
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOKS
    // --------------------------------------------------------------------------------------------------
    public function addBook_post()
    {
        $data = [
            'book_name' => $this->post('book_name'),
            'book_category' => $this->post('book_category'),
            'book_industry' => $this->post('book_industry'),
            'author' => $this->post('author'),
            'date_uploaded' => $this->post('date_uploaded'),
            'cover_book' => $this->post('cover_book'),
            'book_file' => $this->post('book_file'),
            'description' => $this->post('description'),
        ];


        if ($this->I->addBook($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }
    // END BOOKS
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function addCategory_post()
    {
        $data = [
            'category_name' => $this->post('category_name'),
        ];


        if ($this->I->addCategory($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function addIndustry_post()
    {
        $data = [
            'industry_name' => $this->post('industry_name'),
        ];


        if ($this->I->addIndustry($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function addRole_post()
    {
        $data = [
            'role_name' => $this->post('role_name'),
        ];


        if ($this->I->addRole($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------

    // MESSAGE
    // --------------------------------------------------------------------------------------------------
    public function addMessage_post()
    {
        $data = [
            'full_name' => $this->post('full_name'),
            'email' => $this->post('email'),
            'message' => $this->post('message'),
            'status' => $this->post('status')
        ];


        if ($this->I->addMessage($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }
    // END MESSAGE
    // --------------------------------------------------------------------------------------------------

    // LIKE & DISLIKE
    // --------------------------------------------------------------------------------------------------
    public function likeNDislike_post()
    {

        // get ud and bd
        $user_id = $this->post('user_id');
        $book_id = $this->post('book_id');

        // $this->db->where('user_id', $user_id);
        // $this->db->where('book_id', $book_id);
        // $query = $this->db->get('likes');

        $query = "SELECT * FROM likes
                    WHERE user_id = $user_id
                    AND book_id = $book_id";

        $result = $this->db->query($query)->num_rows();

        // var_dump($result);
        // die;
        $data = [
            'user_id'     => $user_id,
            'book_id'     => $book_id,
            'like_status' => 1
        ];

        if ($result > 0) {
            if ($this->D->likeNDislike($data) > 0) {
                $count_like_book = $this->db->get_where('likes', ['book_id' => $book_id])->num_rows();
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'likes' => $count_like_book,
                    'message' => 'Data berhasil dihapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        } else {
            if ($this->I->likeNDislike($data) > 0) {
                $count_like_book = $this->db->get_where('likes', ['book_id' => $book_id])->num_rows();
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'likes' => $count_like_book,
                    'message' => 'Data berhasil ditambahkan'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data gagal ditambahkan'
                ], 404);
            }
        }
    }

    public function bookmark_post()
    {

        // get ud and bd
        $user_id = $this->post('user_id');
        $book_id = $this->post('book_id');

        // $this->db->where('user_id', $user_id);
        // $this->db->where('book_id', $book_id);
        // $query = $this->db->get('likes');

        $query = "SELECT * FROM bookmark
                    WHERE user_id = $user_id
                    AND book_id = $book_id";

        $result = $this->db->query($query)->num_rows();

        // var_dump($result);
        // die;
        $data = [
            'user_id' => $user_id,
            'book_id' => $book_id
        ];

        if ($result > 0) {
            if ($this->D->bookmark($data) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $data,
                    'message' => 'Data berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        } else {
            if ($this->I->bookmark($data) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'Data berhasil ditambahkan'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data gagal ditambahkan'
                ], 404);
            }
        }
    }
    // END LIKE & DISLIKE
    // --------------------------------------------------------------------------------------------------
}
