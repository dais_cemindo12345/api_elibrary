<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Update extends RestController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Update_model', 'U');
    }

    // USER
    // --------------------------------------------------------------------------------------------------
    public function updateUser_post()
    {
        $data = [
            'user_id' => $this->post('user_id'),
            'username' => $this->post('username'),
            'email' => $this->post('email'),
            'phone_number' => $this->post('phone_number'),
            'user_image' => $this->post('user_image'),
            'date_created' => $this->post('date_created'),
            'user_created' => $this->post('user_created'),
            'role_id' => $this->post('role_id'),
        ];


        if ($this->U->updateUser($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data user berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data user gagal diubah'
            ], 404);
        }
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOKS
    // --------------------------------------------------------------------------------------------------
    public function updateBook_post()
    {
        $data = [
            'book_id' => $this->post('book_id'),
            'book_name' => $this->post('book_name'),
            'book_category' => $this->post('book_category'),
            'book_industry' => $this->post('book_industry'),
            'author' => $this->post('author'),
            'description' => $this->post('description'),
        ];

        if ($this->U->updateBook($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }
    // END BOOKS
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function updateCategory_post()
    {
        $data = [
            'category_id' => $this->post('category_id'),
            'category_name' => $this->post('category_name')
        ];


        if ($this->U->updateCategory($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function updateIndustry_post()
    {
        $data = [
            'industry_id' => $this->post('industry_id'),
            'industry_name' => $this->post('industry_name')
        ];


        if ($this->U->updateIndustry($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function updateRole_post()
    {
        $data = [
            'role_id' => $this->post('role_id'),
            'role_name' => $this->post('role_name')
        ];


        if ($this->U->updateRole($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function updateMessage_post()
    {
        $data = [
            'message_id' => $this->post('message_id'),
            'full_name' => $this->post('full_name'),
            'email' => $this->post('email'),
            'message' => $this->post('message'),
            'status' => $this->post('status')
        ];


        if ($this->U->updateMessage($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------
}
