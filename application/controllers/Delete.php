<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Delete extends RestController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Delete_model', 'D');
    }

    // USER
    // --------------------------------------------------------------------------------------------------
    public function deleteUser_post()
    {
        $id = $this->post('user_id');

        if ($id == null) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Tidak ditemukan'
            ], 400);
        } else {
            if ($this->D->deleteUser($id) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $id,
                    'message' => 'User berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'User tidak ditemukan'
                ], 404);
            }
        }
    }
    // END USER
    // --------------------------------------------------------------------------------------------------

    // BOOK
    // --------------------------------------------------------------------------------------------------
    public function deleteBook_post()
    {
        $id = $this->post('book_id');

        if ($id == null) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Tidak ditemukan'
            ], 400);
        } else {
            if ($this->D->deleteBook($id) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $id,
                    'message' => 'Data berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        }
    }
    // END BOOK
    // --------------------------------------------------------------------------------------------------

    // CATEGORY
    // --------------------------------------------------------------------------------------------------
    public function deleteCategory_post()
    {
        $id = $this->post('category_id');

        if ($id == null) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Tidak ditemukan'
            ], 400);
        } else {
            if ($this->D->deleteCategory($id) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $id,
                    'message' => 'Data berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        }
    }
    // END CATEGORY
    // --------------------------------------------------------------------------------------------------

    // INDUSTRY
    // --------------------------------------------------------------------------------------------------
    public function deleteIndustry_post()
    {
        $id = $this->post('industry_id');

        if ($id == null) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Tidak ditemukan'
            ], 400);
        } else {
            if ($this->D->deleteIndustry($id) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $id,
                    'message' => 'Data berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        }
    }
    // END INDUSTRY
    // --------------------------------------------------------------------------------------------------

    // ROLE
    // --------------------------------------------------------------------------------------------------
    public function deleteRole_post()
    {
        $id = $this->post('role_id');

        if ($id == null) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Tidak ditemukan'
            ], 400);
        } else {
            if ($this->D->deleteRole($id) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $id,
                    'message' => 'Data berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        }
    }
    // END ROLE
    // --------------------------------------------------------------------------------------------------

    // MESSAGE
    // --------------------------------------------------------------------------------------------------
    public function deleteMessage_post()
    {
        $id = $this->post('message_id');

        if ($id == null) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Tidak ditemukan'
            ], 400);
        } else {
            if ($this->D->deleteMessage($id) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $id,
                    'message' => 'Data berhasil hapus'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data tidak ditemukan'
                ], 404);
            }
        }
    }
    // END MESSAGE
    // --------------------------------------------------------------------------------------------------
}
